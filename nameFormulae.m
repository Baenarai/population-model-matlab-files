regexF = 'k[0-9][0-9]?';
regexR = 'kr[0-9][0-9]?';
regexC = 'kc[0-9][0-9]?';
regexP = 'kp';
regexD = 'kd';
regexMult = '\*';

reactionsF = [];
reactionsR = [];
reactionsC = [];
reactionsP = [];
reactionsD = [];

formulaeNew = formulae;

for i=1:1:length(formulaeNew)
    [firstF lastF] = regexp(formulaeNew{i,2},regexF);
    [firstR lastR] = regexp(formulaeNew{i,2},regexR);
    [firstC lastC] = regexp(formulaeNew{i,2},regexC);
    [firstP lastP] = regexp(formulaeNew{i,2},regexP);
    [firstD lastD] = regexp(formulaeNew{i,2},regexD);
    
    % Check for type of formula
    if ~isempty(firstF)
        formulaeNew{i,1} = strcat('vf',formulaeNew{i,2}(firstF+1:1:lastF));
        reactionsF(end+1) = i;
    end
    
    if ~isempty(firstR)
        formulaeNew{i,1} = strcat('vr',formulaeNew{i,2}(firstR+2:1:lastR));
        reactionsR(end+1) = i;
    end
    
    if ~isempty(firstC)
        formulaeNew{i,1} = strcat('vc',formulaeNew{i,2}(firstC+2:1:lastC));
        reactionsC(end+1) = i;
    end
    
    if ~isempty(firstP)
        formulaeNew{i,1} = formulaeNew{i,2}(2:1:length(formulaeNew{i,2}));
        reactionsP(end+1) = i;
    end
    
    if ~isempty(firstD)
        [firstMult lastMult] = regexp(formulaeNew{i,2},regexMult);
        formulaeNew{i,1} = strcat('d',formulaeNew{i,2}(firstMult+1:1:length(formulaeNew{i,2})));
        reactionsD(end+1) = i;
    end
    
    % Create declaration string fragment
    formulaeNew{i,3} = strcat('''',formulaeNew{i,1},''':''',formulaeNew{i,2},'''');
    
    % Cleanup
    clear firstF lastF firstR lastR firstC lastC firstP lastP firstD lastD firstMult lastMult;
end

% Create declaration strings
stringStart = '{';
stringEnd = '}';

stringF = '';
stringR = '';
stringC = '';
stringP = '';
stringD = '';

for i=1:1:length(reactionsF)
    if i > 1
        stringF = strcat(stringF,formulaeNew{reactionsF(i),3}(1:1:length(formulaeNew{reactionsF(i),3})-1),'/V''');
    else
        stringF = strcat(stringF,formulaeNew{reactionsF(i),3});
    end
    if i < length(reactionsF)
        stringF = strcat(stringF,',');
    end
end
stringF = strcat(stringStart,stringF,stringEnd);

for i=1:1:length(reactionsR)
    stringR = strcat(stringR,formulaeNew{reactionsR(i),3});
    if i < length(reactionsR)
        stringR = strcat(stringR,',');
    end
end
stringR = strcat(stringStart,stringR,stringEnd);

for i=1:1:length(reactionsC)
    stringC = strcat(stringC,formulaeNew{reactionsC(i),3});
    if i < length(reactionsC)
        stringC = strcat(stringC,',');
    end
end
stringC = strcat(stringStart,stringC,stringEnd);

for i=1:1:length(reactionsP)
    stringP = strcat(stringP,formulaeNew{reactionsP(i),3}(1:1:length(formulaeNew{reactionsP(i),3})-1),'*V''');
    if i < length(reactionsP)
        stringP = strcat(stringP,',');
    end
end
stringP = strcat(stringStart,stringP,stringEnd);

for i=1:1:length(reactionsD)
    stringD = strcat(stringD,formulaeNew{reactionsD(i),3});
    if i < length(reactionsD)
        stringD = strcat(stringD,',');
    end
end
stringD = strcat(stringStart,stringD,stringEnd);

% Cleanup
clear regexF regexR regexC regexP regexD regexMult;
%clear reactionsF reactionsR reactionsC reactionsP reactionsD;
clear i stringStart stringEnd;